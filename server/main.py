from server import Server
import sys
import threading

server = Server()


def main():

    if (len(sys.argv) == 3):
        serverThread = threading.Thread(target=startServer, args=(
            int(sys.argv[1]), int(sys.argv[2]),), daemon=True)
    else:
        serverThread = threading.Thread(target=startServer, args=(
            5151, 2,), daemon=True)
    serverThread.start()
    watcherThread = threading.Thread(target=watcherServer)
    watcherThread.start()
    watcherThread.join()
    sys.exit(0)


def startServer(port, cnt):
    server.start(port, cnt)


def watcherServer():
    while True:
        cmd = input()
        if cmd == "stop":
            server.stop()
            break


if __name__ == '__main__':
    main()
