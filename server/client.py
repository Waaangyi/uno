import jsonpickle
import logging


class Client:
    def __init__(self, sock):
        logging.basicConfig(level=logging.DEBUG)
        self.sock = sock

    def recv(self):
        msg = self.sock.recv(4096).decode('utf-8')
        msg = jsonpickle.decode(msg)
        logging.info("RECV: " + str(msg))
        return msg

    def send(self, msg):
        self.sock.send(bytes(jsonpickle.encode(msg), 'utf-8'))
        logging.info("SEND: " + str(msg))

    def close(self):
        self.sock.close()
