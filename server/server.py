import socket
import threading
import logging
import jsonpickle
from game import Game
from client import Client
import time

HOST = "0.0.0.0"


class Server:
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        pass

    def start(self, PORT, playerCnt):
        logging.basicConfig(level=logging.DEBUG)
        logging.info("server starting")
        self.sock.bind((HOST, PORT))
        self.sock.listen()
        self.playerCnt = playerCnt
        logging.info("server started and listening")

        logging.info("waiting on clients to join")
        self.waitJoin()

        logging.info("waiting on clients to be ready")
        ready = 0
        self.sock.settimeout(1)
        while ready != self.playerCnt:
            for c in self.clients:
                try:
                    if c.recv() == "ready":
                        c.send("acknowledged")
                        ready += 1
                    logging.info(str(ready) + " players ready")
                except:
                    pass
            time.sleep(1)
        self.sock.settimeout(None)

        logging.info("starting game")
        self.game = Game(playerCnt)
        self.game.initDrawStack()
        self.game.initPlayers()
        self.game.initDiscardStack()

        logging.info("sending card data to players")
        for i, e in enumerate(self.game.playerStack):
            self.clients[i].send(e)
        self.play()

    def play(self):
        while True:
            logging.info("Sending discard stack top")
            # Send clients movement information
            logging.info(self.game.index)
            for i in range(self.playerCnt):
                self.clients[i].send(self.game.playerStack[i])
                time.sleep(.2)
                self.clients[i].send(self.game.getTopDiscard())
                time.sleep(.2)
                if i == self.game.index:
                    self.clients[i].send(True)
                else:
                    self.clients[i].send(False)
            logging.info("Waiting for client")
            while True:
                msg = self.clients[self.game.index].recv()
                if msg["Status"] == "draw card":
                    # Draw Card
                    logging.info("Client requested to draw a card")
                    self.game.drawCards(1)
                    self.clients[self.game.index].send(
                        {"Status": True, "Stack": self.game.playerStack[self.game.index]})
                elif msg["Status"] == "play card":
                    card = msg["Card"]
                    index = self.game.index
                    ret = self.game.move(card)
                    if ret:
                        time.sleep(.1)

                        # Prompt for color if wildcard
                        if card.isWildCard:
                            self.clients[index].send(
                                {"Status": "color", "Stack": self.game.getPlayerStack(index)})
                            self.changeColor(index)
                        else:
                            self.clients[index].send(
                                {"Status": True, "Stack": self.game.getPlayerStack(index)})

                        break
                    else:
                        self.clients[index].send({"Status": False})

    def changeColor(self, index):
        while True:
            msg = self.clients[index].recv()
            if self.game.setColor(msg["Color"]):
                self.clients[index].send({"Status": True})
                break
            else:
                self.clients[index].send({"Status": False})

    def checkName(self, name):
        for c in self.clients:
            if c.name == name:
                return False
        return True

    def getNameList(self):
        ret = []
        for c in self.clients:
            ret.append(c.name)
        return ret

    def waitJoin(self):
        self.clients = []
        while len(self.clients) != self.playerCnt:
            c, addr = self.sock.accept()
            client = Client(c)
            client.name = client.recv()
            # Name test
            if not self.checkName(client.name):
                for i in range(1, 100):
                    if not self.checkName(client.name + str(i)):
                        pass
                    else:
                        client.name += str(i)
                        break
            # send the name to client, rename if necessary
            client.send(client.name)

            # send the client the current name list
            client.send(self.getNameList())

            for c in self.clients:
                try:
                    c.send("NC " + client.name)
                except Exception as e:
                    logging.error(
                        "Some error occured while trying to send the new connection event: " + e)

            self.clients.append(client)
            logging.info(client.name +
                         " (" + str(addr) + ") joined the game")
        # Send accept ready signal

        # It appears there is a race condition here, so a minor delay is necessary
        time.sleep(3)
        for c in self.clients:
            c.send("ready")

    def stop(self):
        logging.info("Closing all client connections")
        for client in self.clients:
            client.close()

        logging.info("Closing the server sock")

        self.sock.close()
        logging.info("Goodbye!")
