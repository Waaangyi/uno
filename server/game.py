from card import Card
import logging
import random


class Game:
    def __init__(self, playerCnt):
        self.drawStack = []
        self.discardStack = []
        self.playerStack = []
        self.playerCnt = playerCnt
        self.index = 0
        self.direction = 1

    def printPlayer(self, index):
        for i in self.playerStack[index]:
            print(i)

    def initDrawStack(self):
        for i in range(10):
            self.drawStack.append(Card("r", str(i)))
            self.drawStack.append(Card("g", str(i)))
            self.drawStack.append(Card("b", str(i)))
            self.drawStack.append(Card("y", str(i)))
        for i in range(1, 10):
            self.drawStack.append(Card("r", str(i)))
            self.drawStack.append(Card("g", str(i)))
            self.drawStack.append(Card("b", str(i)))
            self.drawStack.append(Card("y", str(i)))
        for i in range(2):
            self.drawStack.append(Card("r", "+2"))
            self.drawStack.append(Card("g", "+2"))
            self.drawStack.append(Card("b", "+2"))
            self.drawStack.append(Card("y", "+2"))
            self.drawStack.append(Card("r", "skip"))
            self.drawStack.append(Card("g", "skip"))
            self.drawStack.append(Card("b", "skip"))
            self.drawStack.append(Card("y", "skip"))
            self.drawStack.append(Card("r", "reverse"))
            self.drawStack.append(Card("g", "reverse"))
            self.drawStack.append(Card("b", "reverse"))
            self.drawStack.append(Card("y", "reverse"))
        for i in range(4):
            self.drawStack.append(Card("w", "multicolor"))
            self.drawStack.append(Card("w", "+4"))
        random.shuffle(self.drawStack)

    def initDiscardStack(self):
        self.discardStack.append(self.drawStack.pop())
        while self.discardStack[-1].isWildCard:
            self.discardStack.append(self.drawStack.pop())
        self.topColor = self.discardStack[-1].color

    def initPlayers(self):
        # Create player slots
        for _ in range(self.playerCnt):
            self.playerStack.append([])

        # Add cards
        for _ in range(7):
            for i in self.playerStack:
                i.append(self.drawStack.pop())

    def resetDrawStack(self):
        temp = self.discardStack.pop()
        random.shuffle(self.discardStack)
        self.drawStack = self.discardStack
        self.discardStack.clear()
        self.discardStack.append(temp)

    def isLegal(self, other):
        # Same color
        if self.topColor == other.color:
            return True

        # Same value
        if self.discardStack[-1].value == other.value:
            return True

        # Wild card
        if other.isWildCard:
            return True

        return False

    def drawCards(self, cnt):
        while cnt != 0:
            if len(self.drawStack) == 0:
                logging.error("drawStack out of cards")
                return 1
            self.playerStack[self.index].append(self.drawStack.pop())
            if (len(self.drawStack) == 0):
                self.resetDrawStack()
            cnt -= 1

    def getTopDiscard(self):
        return self.discardStack[-1]

    def getPlayerStack(self, index):
        return self.playerStack[index]

    def getNextIndex(self):
        self.index += self.direction
        if (self.index < 0):
            self.index = self.playerCnt - 1
        if (self.index == self.playerCnt):
            self.index = 0

    def move(self, other):
        # If the card is legal
        if not self.isLegal(other):
            return False

        # Check if card is in the player's stack
        if other not in self.playerStack[self.index]:
            return False

        # Add to discardStack
        self.discardStack.append(other)

        # Remove from player's Stack
        self.playerStack[self.index].remove(other)

        # Change the topColor
        self.topColor = other.color

        if other.value == "+2":
            logging.info("Draw 2 cards")
            logging.info("PREV len: " + str(len(self.playerStack[self.index])))
            self.getNextIndex()
            self.drawCards(2)
            logging.info("len: " + str(len(self.playerStack[self.index])))

        if other.value == "+4":
            logging.info("Draw 4 cards")
            logging.info("PREV len: " + str(len(self.playerStack[self.index])))
            self.getNextIndex()
            self.drawCards(4)
            logging.info("len: " + str(len(self.playerStack[self.index])))

        if (other.value == "reverse"):
            self.direction = -self.direction

        if (other.value == "skip"):
            self.getNextIndex()

        self.getNextIndex()
        return True

    def setColor(self, color):
        if color[0].lower() in ["r", "g", "b", "y"]:
            self.topColor = color[0].lower()
            return True
        else:
            return False
