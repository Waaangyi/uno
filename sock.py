import socket
import jsonpickle
import logging


class Sock:
    def __init__(self, HOST, PORT, debug):
        self.debug = debug
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.HOST = HOST
        self.PORT = PORT

    def connect(self):
        self.sock.connect((self.HOST, self.PORT))

    def send(self, msg):
        self.sock.send(bytes(jsonpickle.encode(msg), 'utf-8'))
        if self.debug:
            logging.info("Sent: " + str(msg))

    def sendJson(self, msg):
        msg = jsonpickle.encode(msg)
        self.send(msg)

    def recv(self):
        msg = jsonpickle.decode(self.sock.recv(4096).decode('utf-8'))
        if self.debug:
            logging.info("Recv: " + str(msg))
        return msg

    def recvJson(self):
        return jsonpickle.decode(self.recv())
