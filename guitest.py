from PyQt5 import QtCore, QtGui, QtWidgets, uic
#from PyQt5.QtWidgets import QMessageBox
import logging

from client import Client


class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        uic.loadUi('connectgui.ui', self)
        self.joinButton.setCheckable(True)
        self.joinButton.clicked.connect(self.join)

    def join(self):
        ip = self.serverIP.text()
        port = self.serverPort.text()
        name = self.name.text()
        if len(ip) == 0 or len(port) == 0 or len(name) == 0:
            return False

        client = Client(ip, int(port), name)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())
