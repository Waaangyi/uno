import socket
import threading
import logging
import jsonpickle
import time
from sock import Sock


class Client:
    def __init__(self, HOST, PORT, name):
        logging.basicConfig(level=logging.DEBUG)
        debug = False
        self.sock = Sock(HOST, PORT, debug)
        self.HOST = HOST
        self.PORT = PORT
        self.name = name
        logging.info("Connecting to %s", self.HOST)
        while True:
            try:
                self.sock.connect()
                logging.info("Connected!")
                break
            except:
                logging.error("Server is not up, retrying...")
                time.sleep(1)

        logging.info("Trying to enter game with name %s", self.name)
        self.sock.send(self.name)
        self.name = self.sock.recv()
        logging.info("Server acknowledged with name %s", self.name)
        logging.info("Getting player names")

        self.nameList = self.sock.recv()
        logging.info(self.nameList)
        self.nameList.append(self.name)

        logging.info("Waiting for everyone to join")
        while True:
            msg = self.sock.recv()
            if (msg == "ready"):
                break
            name = msg[2:]
            logging.info(name + " Joined the game")
            self.nameList.append(msg[2:])

        logging.info("Server is full, waiting on client ready signal")

    def ready(self):
        self.sock.send("ready")
        logging.info("Sent ready singal to server")
        self.sock.recv()
        # Receive stack information
        self.myStack = self.sock.recv()
        logging.info(self.myStack)

    def getTurn(self):
        self.myStack = self.sock.recv()
        self.topDiscard = self.sock.recv()
        turn = self.sock.recv()
        return self.topDiscard, self.myStack, turn

    def playCard(self, card):
        self.sock.send({"Status": "play card", "Card": card})
        ret = self.sock.recv()
        if ret["Status"] != False:
            self.myStack = ret["Stack"]
        return ret["Status"]

    def setColor(self, color):
        self.sock.send({"Color": color})
        return self.sock.recv()["Status"]

    def drawCard(self):
        self.sock.send({"Status": "draw card"})
        msg = self.sock.recv()
        if msg["Status"] == True:
            return msg["Stack"]
        else:
            logging.error(
                "Something went wrong while asking the server for new cards")


def start(HOST, PORT, name):
    client = Client(HOST, PORT, name)
