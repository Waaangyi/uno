from client import Client
from card import Card

HOST = input("Ip: ")
PORT = int(input("Port: "))
name = input("Name:")

client = Client(HOST, PORT, name)

input("press any key to be ready")
client.ready()

myOldStack = []
while True:
    topDiscard, myStack, turn = client.getTurn()
    print("Your stack: ")
    for c in myStack:
        print(c)
    print("Discard stack top: " + str(topDiscard))
    if turn:
        print("It's now your turn")
        while True:
            s = input("Card: ")
            if s.lower() == "d":
                # Draw card
                myStack = client.drawCard()
                print("Your stack: ")
                for c in myStack:
                    print(c)
                print("Discard stack top: " + str(topDiscard))
            else:
                s = s.split()
                if (len(s) != 2):
                    print("Malformed input")
                else:
                    card = Card(s[0], s[1])
                    ret = client.playCard(card)
                    if ret == False:
                        print("Malformed input")
                    else:
                        if ret == "color":
                            while True:
                                c = input("Enter Color: ")
                                if client.setColor(c):
                                    break
                                else:
                                    print("Bad color")
                        break
